require 'net/http'

=begin
We can structure this modally by having different prompts for
each mode and using return statements to switch between
(although this would be deviating from the assignment).
See ex43.rb in "Learn Ruby the Hard Way"
=end

class Page

	attr_accessor :uri, :content, :keyword # these getter/setter methods are for debugging purposes

	def initialize
		# TODO
	end

	def get_uri()
		# use gets and return a string uri
		# TODO
	end

	def fetch_content()
		@content = Net::HTTP.get(URI(@uri))
		return @content
	end

	def search_by_keyword(keyword)
		# TODO
	end
end