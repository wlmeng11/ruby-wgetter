require_relative 'page'
require_relative 'prompt'
require 'net/http'

=begin
List of functions needed:
- Print prompt
- Get user input (and check for exit sequence)
- Parse user input into URI and keyword
- Fetch HTML content from given URI
- Search for the keyword within the HTML content

Control Flow Proposal:
- Create new instance of Prompt in app.rb
- The Prompt object handles the looping of the prompt
  and getting user input.
- It then passes the input that it received to a Page object
  and makes calls to all the functions there.

Alternative Control Flow:
- Instantiate Prompt and Page in app.rb
- Loop in app.rb makes calls to Prompt object to display prompt
  and get user input
- Pass user input to Page, which parses the input into a URI and keyword
  (alternatively, parse in Prompt and pass URI & keyword to Page)
=end

p = Prompt.new()

while true
    input = p.prompt()
    
    if input == nil
        break
    elsif input.empty?
        next
    end
    
	# Page stuff
end