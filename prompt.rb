class Prompt

	def initialize()
		# probably won't need this
	end

	def prompt()
		print "> "
		get_input()
		if(quit?)
			return nil
		end
        
        @user_input_split = @user_input.rstrip.lstrip.split(",").map()
        
		return @user_input_split
	end

	def get_input()
		@user_input = gets
	end

	def quit?()
		if @user_input == nil
			puts "exit"
			return true
		elsif @user_input.rstrip.lstrip == "exit"
			return true
		elsif @user_input.rstrip.lstrip == "quit"
			return true
		else
			return false
		end
	end
=begin
rstrip removes whitespace on the right side of a string
kinda like chomp(), but also removes spaces and tabs.
Appending "!" to the method will have it run in-place.
"uri.rstrip!" is equivalent to "uri = uri.rstrip"
Just doing "uri.rstrip" does not modify "uri" itself.

We use rstrip because the user will hit the enter key after text,
so instead of comparing to "exit\n", we can just compare to "exit".

lstrip does the same thing, except on the left side
=end
end